

class User {
  String id;
  String user;
  String password;
  String status;
  String key;
  String keyPost;
  String title;
  String content;
  String color;
  String special;
  String target;
  String date;
  String keyUser;
  String dateDelete;

  User(String id, String title, String img ) {
    this.id = id;
    this.user = title;
    this.password = img;
  }


  User.fromJson(Map json)
        : id        = json['id'],
        user        = json['user'],
        password    = json['password'],
        status      = json['status'],
        key         = json['keyuser'],
        keyPost     = json['keyPost'],
        title       = json["title"],
        content     = json["content"],
        color       = json["color"],
        special     = json["special"],
        target      = json["target"],
        date        = json["date"],
        keyUser     = json["keyuser"],
        dateDelete  = json["dateDelete"];


}