import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:twono/sql/ClientModel.dart';

const key = 'mykey';

var baseUrl = 'myDomain.com' ;


class Api {

  //login
  static Future sendAndGet(var password , var user) async {
    var s = latin1.encode(password);
    var base64encoded = base64.encode(s);
    var url = '$baseUrl/login.php?key=$key&user=$user&password=$base64encoded';
      return httpSend(url);
  }

  //Sing UP
  static Future sendDataForSingUp(var password , var user,var email ,var name) async {

    var s = latin1.encode(password);
    var base64encoded = base64.encode(s);
    var url = '$baseUrl/singUp.php?key=$key&name=$name&user=$user&password=$password&email=$email';

    return httpSend(url);

  }

  //delete Note
  static Future sendDateForDelete( Client newClient) async {

    var url = '$baseUrl/deleteNote.php?key=$key&keyPost=${newClient.keyPost}&date=${newClient.dateDelete}';

    return httpSend(url);

  }

  //send Note(Post) to server
  static Future sendDataNote(Client newClient) async {
    var url = '$baseUrl/addNote.php?key=$key&keyuser=${newClient.keyUser}&title=${newClient.title}&text=${newClient.content}&color=${newClient.color}&special=${newClient.special}&target=${newClient.target}&date=${newClient.date}&keyPost=${newClient.keyPost}';

    return httpSend(url);

  }

  //get key for Post
  static Future getKeyPost(var keyUser) async {
    var url = '$baseUrl/key.php?key=$key&keyuser=$keyUser';
    return httpSend(url);
  }


  static Future getData(var keyUser) async {
    var url = '$baseUrl/getNote.php?key=$key&keyuser=$keyUser';
    return httpSend(url);
  }

  //http :)
  static Future httpSend(var url) async{
    var response;

    try {
      response = await http.get(url);
    }catch(e){
      print(e);
    }

    if (response.statusCode == 200) {
      print('send data http');
      return response;

    }
    else print("Request failed with status: ${response.statusCode}.");

  }


}


