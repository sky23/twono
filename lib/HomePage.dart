import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:twono/page/search/search.dart';
import 'package:twono/page/user/HomeUser.dart';
import 'package:twono/widget/header.dart';
import 'package:twono/page/AddPost.dart';
import 'package:twono/widget/List.dart';
import 'package:twono/sql/Database.dart';
import 'package:twono/sql/ClientModel.dart';
import 'api/server.dart';
import 'api/user.dart';

int idWeekDay = 0;
String special;

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var dataNotKeyPost = new List<Client>();
  var dataDelete = new List<Client>();
  var getInfoKeyPost = new List<User>();
  var getPost = new List<User>();

  var key;
  String keyPost;
  var net = false;
  var sendDate = true;
  var check = true;
  static final oneDecimal = const Duration(seconds: 5);

  //check Login user
  _incrementCounter() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      key = prefs.getString('key') ?? null;
    });
  }

  //get Key For Post from server
  _getKeyPost(keyUser) {
    //login USer and send Data
    Api.getKeyPost(keyUser).then((responseTwo) {
      if (this.mounted) {
        setState(() {
          Iterable listed = json.decode(responseTwo.body);
          getInfoKeyPost = listed.map((model) => User.fromJson(model)).toList();
        });
      }
    });
  }

  //get posts from server
  _getData(keyUser) {
    //login USer and send Data
    Api.getData(keyUser).then((responseTwo) {
      if (this.mounted) {
        setState(() {
          Iterable listed = json.decode(responseTwo.body);
          getPost = listed.map((model) => User.fromJson(model)).toList();
        });
      }
    });
  }

  //get from sql
  getDataNotKey() async {
    dataNotKeyPost = await DBProvider.db.getNotKeyPosts();
    dataDelete = await DBProvider.db.getPostDelete();

    print(dataNotKeyPost.length);
    print('run sql');
  }

  sendDataNotKey(Client newClient) async {
    Api.sendDataNote(newClient);
  }

  deletePostsInServer(Client newClient) async {
    Api.sendDateForDelete(newClient);
  }

  //check connected to Internet => connected => ping google.com
  Future checkNet() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        setState(() => net = true);
      }
    } on SocketException catch (_) {
      setState(() => net = false);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _incrementCounter();
    checkNet();
  }


  sendData(){
    if (net) {
      if (key != null && check) {
        dataNotKeyPost.length == 0 ? getDataNotKey() : null;
        getInfoKeyPost.length == 0 ? _getKeyPost(key) : null;
        setState(() {
          check = false;
        });
      }
    }

    if (sendDate && key !=null) {
      if (getInfoKeyPost.length != 0 && dataNotKeyPost.length != 0) {
        print(getInfoKeyPost.length);
        if (getInfoKeyPost.first.keyPost.isNotEmpty) {
          for (int i = 0; dataNotKeyPost.length > i; i++) {
            print(
                'ok $i ${getInfoKeyPost.first.keyPost}${dataNotKeyPost[i].id}');

            Client rnx = Client(
                id: dataNotKeyPost[i].id,
                keyUser: key,
                title: dataNotKeyPost[i].title,
                content: dataNotKeyPost[i].content,
                color: dataNotKeyPost[i].color,
                special: dataNotKeyPost[i].special,
                target: dataNotKeyPost[i].target,
                date: dataNotKeyPost[i].date,
                keyPost:
                '${getInfoKeyPost.first.keyPost}${dataNotKeyPost[i].id}');

            sendDataNotKey(rnx);

            DBProvider.db.updateKeyPost(rnx);

            if (dataNotKeyPost.length - 1 == i) {
              setState(() {
                sendDate = false;
              });
            }
          }
        }
      }

      if (dataDelete.length != 0) {
        for (int i = 0; dataDelete.length > i; i++) {
          if (dataDelete.length != 0 && dataDelete[i].keyPost.isNotEmpty) {
            print('delete ok ${dataDelete.length}');

            Client deleteRnx = Client(
                id: dataDelete[i].id,
                dateDelete: dataDelete[i].dateDelete,
                keyPost: dataDelete[i].keyPost);

            DBProvider.db.deleteClient(id: dataDelete[i].id,
                keyPosts: dataDelete[i].keyPost);

            deletePostsInServer(deleteRnx);
          }

          if (dataDelete.length - 1 == i) {
            sendDate = false;
          }
        }
      }
    }

  }

  getDataFromServer(){
    if(net && key != null) {
      getPost.clear();
      _getData(key) ;
    }
  }

  saveDataInSql() async{

    if(getPost.length != 0){


      for(int i = 0 ; getPost.length >i ; i++){
        print(getPost.length);
        print(i);
        DBProvider.db.deleteClientAllInServer(getPost[i].keyPost)??print('null');
        Client rnd = Client(id: 1 ,title: '${getPost[i].title??' .. '}', content: "${getPost[i].content ??' ..'} ", color: getPost[i].color ,special: getPost[i].special ,target: getPost[i].target ,date: getPost[i].date ,keyUser: key,keyPost: getPost[i].keyPost);
        await DBProvider.db.newClient(rnd);

        if(getPost.length - 1 == i){
          print('sala');
          getPost.clear();
          Navigator.of(context).pop();
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => HomePage()));

        }

      }



    }
  }

  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

    saveDataInSql();
    sendData();

    var deviceSize = MediaQuery.of(context).size;
    var size20Height = deviceSize.height * 0.03125 ;

    return Scaffold(
      key: _scaffoldKey,
      drawer: AppDrawer(),
      floatingActionButton: Padding(
        child: new FloatingActionButton(
            onPressed: () {
              setState(() {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) => AddPost()));
              });
            },
            child: Icon(Icons.add)),
        padding: EdgeInsets.only(bottom: 25.0, left: 25.0),
      ),
      body: SingleChildScrollView(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Stack(children: <Widget>[
            header(context, height: size20Height*11 -10, title: 'یاداشت برداری سریع'),
            Padding(
              padding: EdgeInsets.only(top: 30.0, right: 5.0),
              child: IconButton(
                icon: Icon(
                  Icons.list,
                  color: Colors.white,
                  size: 30.0,
                ),
                onPressed: () {
                  _scaffoldKey.currentState.openDrawer();
                },
              ),
            ),
            Positioned(
              top: size20Height *5,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  // WeekDay
                  Container(
                      //              40                350
                      height: size20Height * 2 , width: size20Height*17.5, child: listWeekDay(context)),

                  // menu : setting , login ,
                  Card(
                    margin: EdgeInsets.only(right: 5.0, left: 5.0),
                    child: Container(
                      height: size20Height * 2.5,
                      width: deviceSize.width - 10.0,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          InkWell(
                            onTap: () => Navigator.of(context).push(
                                MaterialPageRoute(
                                    builder: (context) => HomeUser())),
                            child: SizedBox(
                              height: 50.0,
                              width: 60.0,
                              child: Icon(
                                Icons.person,
                                color: Colors.red,
                                size: 30.0,
                              ),
                            ),
                          ),
                          InkWell(
                            child: SizedBox(
                              height: 50.0,
                              width: 60.0,
                              child: Icon(
                                Icons.bookmark,
                                color: special != 'true'
                                    ? Colors.deepOrangeAccent
                                    : Colors.blueAccent,
                                size: 30.0,
                              ),
                            ),
                            onTap: () {
                              setState(() => special == 'true'
                                  ? special = null
                                  : special = 'true');
                              Navigator.of(context).pop();
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => HomePage()));
                            },
                          ),
                          InkWell(
                            child: SizedBox(
                              height: 50.0,
                              width: 60.0,
                              child: Icon(
                                Icons.search,
                                color: Colors.amber[800],
                                size: 30.0,
                              ),
                            ),
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => SearchPageList()));
                            },
                          ),
                          InkWell(
                            onTap: () {
                              getDataFromServer();
                            },
                            child: SizedBox(
                              height: 50.0,
                              width: 60.0,
                              child: Icon(
                                Icons.replay,
                                color: Colors.amber,
                                size: 30.0,
                              ),
                            ),
                          ),

                        ],
                      ),
                    ),
                  ), //end Card
                ],
              ),
            ),
            Container(
              height: deviceSize.height,
              width: double.maxFinite,
              child: ListContent(
                special: special ?? null,
                idContext: idWeekDay,
              ),
            )
          ]),
        ],
      )),
    );
  }

  Widget listWeekDay(context) {
    var week = [
      'شنبه',
      'یکشنبه',
      'دوشنبه',
      'سه شنبه',
      'چهارشنبه',
      'پنجشنبه',
      'جمعه'
    ];

    var idWeek = [6, 7, 1, 2, 3, 4, 5];

    return ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: 7,
        itemBuilder: (context, index) {
          return Padding(
            padding: EdgeInsets.only(right: 14.0),
            child: InkWell(
              child: new Text(
                week[index],
                style: TextStyle(
                    fontFamily: 'iran',
                    fontSize: 12.0,
                    color: idWeekDay == 0
                        ? idWeek[index] == DateTime.now().weekday
                            ? Colors.deepOrange[100]
                            : Colors.white
                        : idWeekDay == idWeek[index]
                            ? Colors.amber
                            : Colors.white),
                textAlign: TextAlign.center,
              ),
              onTap: () {
                setState(() {
                  idWeekDay == idWeek[index]
                      ? idWeekDay = 0
                      : idWeekDay = idWeek[index];
                  Navigator.of(context).pop();
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => HomePage()));
                });
              },
            ),
          );
        });
  }
}
