import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:twono/api/server.dart';
import 'package:twono/api/user.dart';
import 'package:twono/sql/ClientModel.dart';
import 'package:twono/widget/header.dart';
import 'package:twono/sql/Database.dart';


class AddPost extends StatefulWidget {

  @override
  _AddPostState createState() => _AddPostState();
}

class _AddPostState extends State<AddPost> {
  var colorBtnWeb =  Colors.blueAccent;
  var txtBtnWeb = '(در حال حاضر در سرور ذخیره میشود)';
  var _switchVal = false; // special
  String dropdownValue = 'بدون دسته بندی';
  var title ;
  var content ;
  var color = '0';
  var date = new DateTime.now().weekday;
  var group = 'بدون دسته بندی';
  Color myColor = Colors.black12;
  var dropdownValueDay = 'بدون زمان';
  var dataSend = new List<User>();
  var getInfoKeyPost = new List<User>();
  var net = false;
  var key;

  /*
  * - check key
  *
  * - check connected to Internet User
  * */
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _incrementCounter();

  }


  @override
  Widget build(BuildContext context) {

    if (net) _getKeyPost(key);
    if(key != null)
      checkNet();

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(children: <Widget>[
          header(context,height: 100.0,title: 'اضافه کردن یاداشت',back: true),
        SizedBox(height: 20.0,),
        Card(
            elevation:4.0,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
            child: Container(
              height: 430.0,width: 350.0,
              child: fieldForAdd(context),
          ),),

          MaterialButton(
            onPressed: (){
              setState(() {
                if(colorBtnWeb == Colors.blueAccent){
                colorBtnWeb = Colors.redAccent;
                txtBtnWeb = '(در حال حاضر در سرور ذخیره نمیشود) $date';

                }
                else {
                  colorBtnWeb = Colors.blueAccent;
                  txtBtnWeb = '(در حال حاضر در سرور ذخیره میشود)';
                }
              });
            },
            child: Row(children: <Widget>[
              Icon(Icons.cloud,color: colorBtnWeb,),
              new Text('  ذخیره در وب سرور $txtBtnWeb ',style: TextStyle(fontFamily: 'iran',fontSize: 8.0,color: colorBtnWeb ),)
            ],),
          ),
          SizedBox(height: 100.0,)
        ],),
      ),
    );
  }


  /* set color [red, black12 , green , yellow]*/
  menuChoseColors() {
    var deviceSize = MediaQuery.of(context).size;
    var size20Height = deviceSize.height * 0.03125 ;

    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text('انتخاب رنگ',style: TextStyle(fontFamily: 'iran',fontSize: 14.0,color: Colors.black54 ),textAlign: TextAlign.right,),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(8.0))),
            contentPadding: EdgeInsets.only(top: 10.0,right: 10.0),
            content: Container(
              width: size20Height*15,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[

                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[

                      InkWell(child: new Container(height: 50.0,width: 50.0,color: Colors.redAccent,),
                        onTap: (){
                          Navigator.of(context).pop(false);
                          setState((){
                            myColor = Colors.redAccent;
                            color = '1';

                          });

                          otherSetting();
                        },
                      ),
                      InkWell(child: new Container(height: 50.0,width: 50.0,color: Colors.green,),
                        onTap: (){
                          Navigator.of(context).pop(false);
                          setState((){
                            myColor = Colors.green;
                            color = '2';

                          });
                          otherSetting();
                        },
                      ),
                      InkWell(child: new Container(height: 50.0,width: 50.0,color: Colors.black12,),
                        onTap: (){
                          Navigator.of(context).pop(false);
                          setState((){
                            myColor = Colors.black12;
                            color = '3';
                          });
                          otherSetting();
                        },
                      ),
                      InkWell(child: new Container(height: 50.0,width: 50.0,color: Colors.amber,),
                        onTap: (){
                          Navigator.of(context).pop(false);
                          setState((){
                            myColor = Colors.amber;
                            color = '4';
                          });
                          otherSetting();
                        },
                      ),

                    ],)
                ],
              ),
            ),
            actions: <Widget>[
              new FlatButton(
                onPressed: () {
                  setState(() =>color = '0');
                  Navigator.of(context).pop(false);
                  otherSetting();
                },
                child: new Text('انصراف', style: TextStyle(fontFamily: 'iran'),textAlign: TextAlign.right,),
              ),
            ],
          );
        })??false;
  }


  /*other setting =>open from page index for add note :
     Special text , text Grouping , Colors =>showDialog => menuChoseColors() **-red , black12 , green , yellow -***/
  otherSetting() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text('تنظیمات بیشتر',style: TextStyle(fontFamily: 'iran',fontSize: 14.0,color: Colors.black54 ),textAlign: TextAlign.right,),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(8.0))),
            contentPadding: EdgeInsets.only(top: 10.0,right: 10.0),
            content: Container(
              width: 400.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[

                  Row(children: <Widget>[
                    Spacer(),
                    new Text('مطلب ويژه',style: TextStyle(fontFamily: 'iran',fontSize: 10.0,color: Colors.redAccent),),
                    Spacer(),
                    Switch(onChanged: (bool val){
                      setState(() {
                        _switchVal = val;
                        Navigator.of(context).pop(false);
                        otherSetting();

                      });
                    },
                      value: _switchVal,
                    ),
                    Spacer(flex: 2,),
                    new Text('انتخاب رنگ',style: TextStyle(fontFamily: 'iran',fontSize: 10.0,color: Colors.redAccent),),
                    Spacer(),
                    InkWell(
                      child: Container(width: 35.0,height: 35.0,color: myColor,),
                      onTap: (){
                        Navigator.of(context).pop(false);
                        menuChoseColors();
                      },
                    ),
                    Spacer(flex: 1,),

                  ],),

                  Row(children: <Widget>[

                    new Text('انتخاب دسته بندی'),

                  ],),
                  Padding(
                    padding: EdgeInsets.all(5.0),
                    child: DropdownButton<String>(
                      value: dropdownValue,
                      onChanged: (String newValue) {
                        setState(() {
                          dropdownValue = newValue;
                          Navigator.of(context).pop(false);
                          otherSetting();
                          setState(() {
                            group = newValue;
                          });
                        });
                      },
                      items: <String>['بدون دسته بندی', ' شخصی', ' کاری', 'یادآوری']
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      })
                          .toList(),
                    ),
                  ),



                  Row(children: <Widget>[

                    new Text('مشخص کردن روز'),

                  ],),
                  Padding(
                    padding: EdgeInsets.all(5.0),
                    child: DropdownButton<String>(
                      value: dropdownValueDay,
                      onChanged: (String newValue) {
                        setState(() {

                          dropdownValueDay = newValue;

                          Navigator.of(context).pop(false);
                          otherSetting();

                          switch(newValue){
                            case ' شنبه' :
                              date = 6;
                              break;
                            case ' یکشنبه':
                              date = 7;
                              break;
                            case 'دوشنبه':
                              date = 1;
                              break;
                            case 'سه شنبه':
                              date = 2;
                              break;
                            case 'چهارشنبه':
                              date = 3 ;
                              break;
                            case 'پنج شنبه':
                              date = 4;
                              break;
                            case 'جمعه':
                              date =5;
                              break;

                          }

                        });
                      },
                      items: <String>['بدون زمان', ' شنبه', ' یکشنبه', 'دوشنبه','سه شنبه','چهارشنبه','پنج شنبه','جمعه']
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      })
                          .toList(),
                    ),
                  ),



                ],
              ),
            ),
            actions: <Widget>[
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('انصراف',textAlign: TextAlign.right,),
              ),
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('ذخیره',textAlign: TextAlign.right,),
              ),

            ],
          );
        })??false;
  }


  //send data (note) to server (Api) = > if key != null
  _addDataInserver(Client newClient) {

    //login USer and send Data
    Api.sendDataNote(newClient).then((responseTwo) {
      if (this.mounted) {
        setState(() {
          Iterable listed = json.decode(responseTwo.body);
          dataSend = listed.map((model) => User.fromJson(model)).toList();
        });
      }
    });


  }


  //check connected to Internet => connected => ping google.com
  Future checkNet() async{
    try {
      final result = await InternetAddress.lookup('hulli.ir');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        setState(()=> net = true);
      }
    } on SocketException catch (_) {
      setState(()=> net = false);
    }
  }

  _getKeyPost(keyUser) {

    //login USer and send Data
    Api.getKeyPost(keyUser).then((responseTwo) {
      if (this.mounted) {
        setState(() {
          Iterable listed = json.decode(responseTwo.body);
          getInfoKeyPost = listed.map((model) => User.fromJson(model)).toList();
        });
      }
    });

  }


  /*Field title and content => add post
  *     send Data to server - save to sql
  * */
  Widget fieldForAdd (context){

    var deviceSize = MediaQuery.of(context).size;
    var size20Height = deviceSize.height * 0.03125 ;

    return  Column(children: <Widget>[
      Column(children: <Widget>[

        Padding(padding: EdgeInsets.all(1.0),
          child: SizedBox(height: 40.0,width: size20Height * 17.5,
            child: new TextField(
              cursorColor: Colors.cyanAccent ,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                labelText: 'موضوع',
                border: OutlineInputBorder(

                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                ),


              ),
              onChanged: (String valName ){
                title = valName.length >35 ? valName.substring(0,35): valName;
              },
            ),
          ),
        ),

        Padding(padding: EdgeInsets.only(top: 8.0,left: 1.0,right: 1.0),
          child:new TextField(
            textDirection: TextDirection.rtl,
            maxLines: 11,
            keyboardType: TextInputType.multiline,
            cursorColor: Colors.cyanAccent,
            decoration: InputDecoration(
              border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),

            ),
            onChanged: (text) {
              setState(() {
                content = text;
              });
            },onSubmitted: (text){
            setState(() {
              content = text;
            });
          },

          ),
        ),


        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            CupertinoButton(
              borderRadius: BorderRadius.all(Radius.circular(28.0)),
              onPressed: (){

                otherSetting();
              },
              child: new Text(' پیشرفته ',style: TextStyle(fontFamily: 'iran',color: Colors.deepOrange[400] ,fontSize: 12.0)),
            ),
            Spacer(flex: 2,),

            //send and save data
            CupertinoButton(
              borderRadius: BorderRadius.all(Radius.circular(28.0)),
              onPressed: () async{
                Client rnd = Client(id: 1 ,title: '${title??' .. '}', content: "${content??' ..'} ", color: color ,special: '$_switchVal' ,target: group ,date: '$date',keyUser: key,keyPost: '${net == true ?getInfoKeyPost[0].keyPost : null}');

                await DBProvider.db.newClient(rnd);
                if(net)_addDataInserver(rnd);
                Navigator.pop(context);

              },
              child: new Text(' ذخیره ',style: TextStyle(fontFamily: 'iran')),
            ),

            CupertinoButton(
              borderRadius: BorderRadius.all(Radius.circular(28.0)),
              onPressed: ()=>Navigator.pop(context),
              child: new Text(' انصراف ',style: TextStyle(fontFamily: 'iran',fontSize: 14.0,color: Colors.redAccent)),
            ),

          ],)

      ],),

    ],);
  }

  // check key (singIn)
  _incrementCounter() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      key = prefs.getString('key') ?? null;
    });
  }
}

