import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:twono/sql/ClientModel.dart';
import 'package:twono/widget/header.dart';
import 'package:twono/sql/Database.dart';


class EditPost extends StatefulWidget {
  final idPost;
  final title;
  final content;
  final color;
  final group;
  final special;
  final data;
  var keyPost;

  EditPost({this.idPost,this.title,this.content,this.color,this.group,this.special,this.data,this.keyPost});

  @override
  _EditPostState createState() => _EditPostState(idPost: idPost,title: title,content: content,color: color,group: group,special: special,data: data,keyPost: keyPost);
}

class _EditPostState extends State<EditPost> {
  var po;
  final idPost;
  var special;
  var colorBtnWeb =  Colors.blueAccent;
  var txtBtnWeb = '(در حال حاضر در سرور ذخیره میشود)';
  var _switchVal ; // special
  String dropdownValue = 'بدون دسته بندی';
  var title ;
  var content ;
  var color = '0';
  var data = '1';
  var group;
  Color myColor ;
  var keyPost;
  var dropdownValueDay = 'بدون زمان';
  final TextEditingController _controllerTitle = TextEditingController();
  final TextEditingController _controllerContent = TextEditingController();

  _EditPostState({this.idPost,this.title,this.content,this.color,this.group,this.special,this.data, this.keyPost});


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controllerTitle.text = title;
    _controllerContent.text = content;
    _switchVal=special=='true'?true:false;
    dropdownValue = group;
    switch(color){
      case '1':myColor = Colors.redAccent;
      break;
      case '2':myColor = Colors.green;
      break;
      case '3':myColor = Colors.black12;
      break ;
      case '4' :myColor = Colors.amber;
      break;
      case '0':myColor = Colors.black12;
      break;
    }


    switch(data){
      case  '6':
        dropdownValueDay = ' شنبه';
        break;
      case '7':
        dropdownValueDay = ' یکشنبه';
        break;
      case '1':
        dropdownValueDay = 'دوشنبه';
        break;
      case '2':
        dropdownValueDay = 'سه شنبه';
        break;
      case '3':
        dropdownValueDay = 'چهارشنبه' ;
        break;
      case '4':
        dropdownValueDay ='پنج شنبه';
        break;
      case '5':
        dropdownValueDay ='جمعه';
        break;

    }


  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(children: <Widget>[
          header(context,height: 100.0,title: 'اضافه کردن یاداشت',back: true),
          SizedBox(height: 20.0,),
          Card(
            elevation:4.0,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
            child: Container(
              height: 430.0,width: 350.0,
              child: fieldForAdd(context),
            ),),

          MaterialButton(
            onPressed: (){
              setState(() {
                if(colorBtnWeb == Colors.blueAccent){
                  colorBtnWeb = Colors.redAccent;
                  txtBtnWeb = '(در حال حاضر در سرور ذخیره نمیشود)';

                }
                else {
                  colorBtnWeb = Colors.blueAccent;
                  txtBtnWeb = '(در حال حاضر در سرور ذخیره میشود)';
                }
              });
            },
            child: Row(children: <Widget>[
              Icon(Icons.cloud,color: colorBtnWeb,),
              new Text('  ذخیره در وب سرور $txtBtnWeb ',style: TextStyle(fontFamily: 'iran',fontSize: 8.0,color: colorBtnWeb ),)
            ],),
          ),
          SizedBox(height: 100.0,)
        ],),
      ),
    );
  }

  /* set color [red, black12 , green , yellow]*/
  menuChoseColors() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text('انتخاب رنگ',style: TextStyle(fontFamily: 'iran',fontSize: 14.0,color: Colors.black54 ),textAlign: TextAlign.right,),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(8.0))),
            contentPadding: EdgeInsets.only(top: 10.0,right: 10.0),
            content: Container(
              width: 300.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[

                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[

                      InkWell(child: new Container(height: 50.0,width: 50.0,color: Colors.redAccent,),
                        onTap: (){
                          Navigator.of(context).pop(false);
                          setState((){
                            myColor = Colors.redAccent;
                            color = '1';

                          });

                          otherSetting();
                        },
                      ),
                      InkWell(child: new Container(height: 50.0,width: 50.0,color: Colors.green,),
                        onTap: (){
                          Navigator.of(context).pop(false);
                          setState((){
                            myColor = Colors.green;
                            color = '2';

                          });
                          otherSetting();
                        },
                      ),
                      InkWell(child: new Container(height: 50.0,width: 50.0,color: Colors.black12,),
                        onTap: (){
                          Navigator.of(context).pop(false);
                          setState((){
                            myColor = Colors.black12;
                            color = '3';
                          });
                          otherSetting();
                        },
                      ),
                      InkWell(child: new Container(height: 50.0,width: 50.0,color: Colors.amber,),
                        onTap: (){
                          Navigator.of(context).pop(false);
                          setState((){
                            myColor = Colors.amber;
                            color = '4';
                          });
                          otherSetting();
                        },
                      ),

                    ],)
                ],
              ),
            ),
            actions: <Widget>[
              new FlatButton(
                onPressed: () {
                  setState(() =>color = '0');
                  Navigator.of(context).pop(false);
                  otherSetting();
                },
                child: new Text('انصراف', style: TextStyle(fontFamily: 'iran'),textAlign: TextAlign.right,),
              ),
            ],
          );
        })??false;
  }


  /*other setting =>open from page index for add note :
     Special text , text Grouping , Colors =>showDialog => menuChoseColors() **-red , black12 , green , yellow -***/
  otherSetting() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text('تنظیمات بیشتر',style: TextStyle(fontFamily: 'iran',fontSize: 14.0,color: Colors.black54 ),textAlign: TextAlign.right,),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(8.0))),
            contentPadding: EdgeInsets.only(top: 10.0,right: 10.0),
            content: Container(
              width: 300.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[

                  Row(children: <Widget>[
                    Spacer(),
                    new Text('مطلب ويژه',style: TextStyle(fontFamily: 'iran',fontSize: 10.0,color: Colors.redAccent),),
                    Spacer(),
                    Switch(onChanged: (bool val){
                      setState(() {
                        _switchVal = val;
                        Navigator.of(context).pop(false);
                        otherSetting();

                      });
                    },
                      value: _switchVal,
                    ),
                    Spacer(flex: 2,),
                    new Text('انتخاب رنگ',style: TextStyle(fontFamily: 'iran',fontSize: 10.0,color: Colors.redAccent),),
                    Spacer(),
                    InkWell(
                      child: Container(width: 35.0,height: 35.0,color: myColor,),
                      onTap: (){
                        Navigator.of(context).pop(false);
                        menuChoseColors();
                      },
                    ),
                    Spacer(flex: 1,),

                  ],),

                  Row(children: <Widget>[

                    new Text('انتخاب دسته بندی'),

                  ],),
                  Padding(
                    padding: EdgeInsets.all(5.0),
                    child: DropdownButton<String>(
                      value: dropdownValue,
                      onChanged: (String newValue) {
                        setState(() {
                          dropdownValue = newValue;
                          Navigator.of(context).pop(false);
                          otherSetting();
                          setState(() {
                            group = newValue;
                          });
                        });
                      },
                      items: <String>['بدون دسته بندی', ' شخصی', ' کاری', 'یادآوری']
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      })
                          .toList(),
                    ),
                  ),
                  Row(children: <Widget>[

                    new Text('مشخص کردن روز'),

                  ],),
                  Padding(
                    padding: EdgeInsets.all(5.0),
                    child: DropdownButton<String>(
                      value: dropdownValueDay,
                      onChanged: (String newValue) {
                        setState(() {

                          dropdownValueDay = newValue;

                          Navigator.of(context).pop(false);
                          otherSetting();

                          switch(newValue){
                            case ' شنبه' :
                              data = '6';
                              break;
                            case ' یکشنبه':
                              data = '7';
                              break;
                            case 'دوشنبه':
                              data = '1';
                              break;
                            case 'سه شنبه':
                              data = '2';
                              break;
                            case 'چهارشنبه':
                              data = '3';
                              break;
                            case 'پنج شنبه':
                              data = '4';
                              break;
                            case 'جمعه':
                              data ='5';
                              break;

                          }

                        });
                      },
                      items: <String>['بدون زمان', ' شنبه', ' یکشنبه', 'دوشنبه','سه شنبه','چهارشنبه','پنج شنبه','جمعه']
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      })
                          .toList(),
                    ),
                  ),

                ],
              ),
            ),
            actions: <Widget>[
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('انصراف',textAlign: TextAlign.right,),
              ),
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('ذخیره',textAlign: TextAlign.right,),
              ),

            ],
          );
        })??false;
  }


  Widget fieldForAdd (context){
    return  Column(children: <Widget>[
      Column(children: <Widget>[

        Padding(padding: EdgeInsets.all(1.0),
          child: SizedBox(height: 40.0,width: 350.0,
            child: new TextField(
              controller: _controllerTitle,
              cursorColor: Colors.cyanAccent ,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                labelText: 'موضوع',
                border: OutlineInputBorder(

                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                ),


              ),
              onChanged: (String valName ){
                title = valName.length >35 ? valName.substring(0,35): valName;
              },
            ),
          ),
        ),
        Padding(padding: EdgeInsets.only(top: 8.0,left: 1.0,right: 1.0),
          child:new TextField(
            controller: _controllerContent,
            textDirection: TextDirection.rtl,
            maxLines: 11,
            keyboardType: TextInputType.multiline,
            cursorColor: Colors.cyanAccent,
            decoration: InputDecoration(
              border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),

            ),
            onChanged: (text) {
              setState(() {
                content = text;
              });
            },onSubmitted: (text){
            setState(() {
              content = text;
            });
          },

          ),
        ),


        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            CupertinoButton(
              borderRadius: BorderRadius.all(Radius.circular(28.0)),
              onPressed: (){

                otherSetting();
              },
              child: new Text(' پیشرفته ',style: TextStyle(fontFamily: 'iran',color: Colors.deepOrange[400] ,fontSize: 12.0)),
            ),
            Spacer(flex: 2,),

            CupertinoButton(
              borderRadius: BorderRadius.all(Radius.circular(28.0)),
              onPressed: () async{
                Client rnd = Client(id: idPost ,title: '${title??' .. '}', content: "${content??' ..'} ", color: color ,special: '$_switchVal' ,target: group ,date: data);
                await DBProvider.db.updateClient(rnd);
                Navigator.pop(context);

              },
              child: new Text(' بروزرسانی ',style: TextStyle(fontFamily: 'iran')),
            ),
            CupertinoButton(
              borderRadius: BorderRadius.all(Radius.circular(28.0)),
              onPressed: ()=>Navigator.pop(context),
              child: new Text(' انصراف ',style: TextStyle(fontFamily: 'iran',fontSize: 14.0,color: Colors.redAccent)),
            ),

          ],)

      ],),

    ],);
  }

}