import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:twono/widget/List.dart';
import 'package:twono/widget/header.dart';

import 'ListSearch.dart';
var word;

class SearchPageList extends StatefulWidget {
  @override
  _SearchPageListState createState() => _SearchPageListState();
}

class _SearchPageListState extends State<SearchPageList> {
  var net = false;



  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SingleChildScrollView(
        child:Column(children: <Widget>[

            Stack(
              children: <Widget>[
                header(context, height: 100.0, title: 'جستوجو'),
                Padding(
                    padding: EdgeInsets.only(top: 100.0),
                    child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 5.0, right: 5.0),
                      child: new TextField(
                        textDirection: TextDirection.rtl,
                        keyboardType: TextInputType.multiline,
                        cursorColor: Colors.cyanAccent,
                        decoration: InputDecoration(
                          labelText: 'کلمه ی مورد نظر خود را وارد کنید',
                        ),
                        onChanged: (String val){
                          setState(() {
                            word = val;
                          });
                        },
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        CupertinoButton(
                          borderRadius: BorderRadius.all(Radius.circular(28.0)),
                          onPressed: () {

                           word == null? null: Navigator.of(context).push(MaterialPageRoute(builder: (context)=> SearchPageListPage(word: word,)));
                          },
                          child: new Text(' جستوجو ',
                              style: TextStyle(
                                  fontFamily: 'iran',
                                  fontSize: 14.0,
                                  color: Colors.blueAccent)),
                        ),
                      ],
                    ),
                  ],
                ),
                ),


              ],
            ),


        ],)

      ),
    );
  }
}
