import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:twono/page/search/search.dart';
import 'package:twono/page/user/HomeUser.dart';
import 'package:twono/widget/header.dart';
import 'package:twono/page/AddPost.dart';
import 'package:twono/widget/List.dart';
import 'package:twono/sql/Database.dart';
import 'package:twono/sql/ClientModel.dart';

import '../../HomePage.dart';


int idWeekDay = 0;
String special;

class SearchPageListPage extends StatefulWidget {
  final word;

  SearchPageListPage({this.word});

  @override
  _SearchPageListPageState createState() => _SearchPageListPageState(word: word);
}

class _SearchPageListPageState extends State<SearchPageListPage> {
  var word;

  _SearchPageListPageState({this.word});

  @override
  Widget build(BuildContext context) {
    var deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Stack(
                  children: <Widget>[
                    header(context, height: 210.0, title: 'جستوجو برای  $word',),

/*
                    Positioned(
                      top: 100.0,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[

                          // menu : setting , login ,
                          Card(
                            margin: EdgeInsets.only(right: 5.0, left: 5.0),
                            child: Container(
                              height: 50.0,
                              width: deviceSize.width - 10.0,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  InkWell(
                                    onTap: ()=>Navigator.of(context).push(MaterialPageRoute(builder: (context)=>HomeUser())),
                                    child:SizedBox(
                                      height: 50.0,
                                      width: 60.0,
                                      child: Icon(
                                        Icons.person,
                                        color: Colors.red,
                                        size: 30.0,
                                      ),
                                    ),
                                  ),
                                  InkWell(
                                    child:SizedBox(
                                      height: 50.0,
                                      width: 60.0,
                                      child: Icon(
                                        Icons.bookmark,
                                        color: special != 'true' ? Colors.deepOrangeAccent : Colors.blueAccent ,
                                        size: 30.0,
                                      ),
                                    ),
                                    onTap: () {

                                      setState(() =>
                                      special == 'true'
                                          ? special = null
                                          : special = 'true');
                                      Navigator.of(context).pop();
                                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => HomePage()));


                                    },
                                  ),

                                  InkWell(
                                    child:                             SizedBox(
                                      height: 50.0,
                                      width: 60.0,
                                      child: Icon(
                                        Icons.search,
                                        color: Colors.amber[800],
                                        size: 30.0,
                                      ),
                                    ),
                                    onTap: (){
                                      Navigator.of(context).push(MaterialPageRoute(builder: (context)=>SearchPageList()));
                                    },
                                  ),

                                  InkWell(
                                    onTap: (){
                                      setState(() {
                                        idWeekDay = 0;
                                      });
                                      Navigator.of(context).pop();
                                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => HomePage()));

                                    },
                                    child: SizedBox(
                                      height: 50.0,
                                      width: 60.0,
                                      child: Icon(
                                        Icons.replay,
                                        color: Colors.amber,
                                        size: 30.0,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 50.0,
                                    width: 60.0,
                                    child: Icon(
                                      Icons.settings,
                                      color: Colors.amberAccent,
                                      size: 30.0,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ), //end Card
                        ],
                      ),
                    ),
                    */
                    Container(
                      height: deviceSize.height,
                      width: double.maxFinite,
                      child: ListContent(idContext: 9,word: word,),
                    )
                  ]
              ),
            ],
          )),
    );
  }

}

