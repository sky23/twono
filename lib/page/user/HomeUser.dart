import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:twono/api/user.dart';
import 'package:twono/page/user/singup.dart';
import 'package:twono/widget/header.dart';
import 'package:twono/api/server.dart';
import 'package:shared_preferences/shared_preferences.dart';

//Home page for chose login or singup
class HomeUser extends StatefulWidget {
  @override
  _HomeUserState createState() => _HomeUserState();
}

class _HomeUserState extends State<HomeUser> {
  var userLogin = new List<User>();
  var password;
  var massage = ' ';
  var user;
  var key;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _incrementCounter(2);
  }

  @override
  Widget build(BuildContext context) {
    print(userLogin.length);


    if(userLogin.length != 0 ?userLogin.first.status == 'Active' : false ){

      key==null ? _incrementCounter(1,keyUser: userLogin.first.key ) : null;

      setState(() {
        massage = ' $key  با موفقیت وارد شدید' ;

      });
    }else if (userLogin.length != 0 ? userLogin.first.status == 'fail' : false ){
      massage ='شکست در ورود به حساب کاربری';
    }

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(children: <Widget>[
          //start Column
          //Start Stack
          Stack(children: <Widget>[
            header(context,title: 'حساب کاربری',height: 220.0),
            Padding(
              padding: EdgeInsets.only(top: 85.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.group,color: Colors.amber,size: 50.0,)
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 130.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Text('خوش امدید\n لطفا وارد حساب کاربری خود شوید',textAlign: TextAlign.center,style: TextStyle(fontFamily: 'iran',color: Colors.white),),
              ],)
            )

          ],),//end stack

          key==null ? fieldForLogin(context) : isLogin() ,


          //end Column
        ],),
      ),
    );
  }


  //logout massage
  Widget isLogin ()=>Column(
    children: <Widget>[
      SizedBox(height: 20.0,),
      new Text('شما قبلا وارد شده اید'),
      RaisedButton(onPressed: ()=>_incrementCounter(5),
        child: new Text('خروج از حساب'),
      )
    ],
  );


  //send data user to server for check login
  _loginUser(var password, var user)  {

    //login USer and send Data
    Api.sendAndGet(password,user).then((responseTwo) {
      if (this.mounted) {
        setState(() {
          massage = 'درحال بررسی اطلاعات .. ';
          Iterable listed = json.decode(responseTwo.body);
          userLogin = listed.map((model) => User.fromJson(model)).toList();
        });
      }
    });
  }


  // form for login (password and user)
  Widget fieldForLogin(context){
    return  Column(children: <Widget>[
      Column(children: <Widget>[

        Padding(padding: EdgeInsets.only(top: 50.0),
          child: SizedBox(height: 60.0,width: 350.0,
            child: new TextField(
              decoration: const InputDecoration(
                  labelText: 'نام کاربری',
                  icon: const Padding(
                      padding: const EdgeInsets.only(
                          top: 15.0),
                      child: const Icon(Icons.account_box)
                  )
              ),
              onChanged: (String valName ){
                setState(() {
                  user = valName.length <= 4 ? null : valName ;
                });
              },
            ),
          ),
        ),

        Padding(padding: EdgeInsets.only(top: 5.0),
          child: SizedBox(height: 60.0,width: 350.0,
            child: new TextField(
              decoration: const InputDecoration(
                  labelText: 'رمزعبور',
                  icon: const Padding(
                      padding: const EdgeInsets.only(
                          top: 15.0),
                      child: const Icon(Icons.lock)
                  )
              ),
              onChanged: (String valName ){

                valName.length <= 6 ? massage = "رمز ضعیف است" : password = valName;

                valName.length>=6? massage=' ' : massage = "رمز ضعیف است";

              },
              obscureText: true,
            ),
          ),
        ),

        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[

            CupertinoButton(
              borderRadius: BorderRadius.all(Radius.circular(28.0)),
              onPressed: (){
                Navigator.pop(context);
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => SingUp()));

              },
              child: new Text(' ایجاد حساب کاربری جدید',style: TextStyle(fontFamily: 'iran',fontSize: 10.0,color: Colors.blue)),
            ),

            CupertinoButton(
              borderRadius: BorderRadius.all(Radius.circular(28.0)),
              onPressed: (){
                setState(() {

                  password ==null || user == null? massage = 'نام کاربری یا رمز وارد نشده': _loginUser(password,user);

                });
                print(userLogin.length);

              },
              child: new Text(' ورود ',style: TextStyle(fontFamily: 'iran',fontSize: 14.0,color: Colors.blueAccent)),
            ),

          ],),


        Padding(
          padding: EdgeInsets.only(top: 10.0),
          child:new Text('$massage' , style: TextStyle(fontFamily: 'iran',color: Colors.redAccent),)
          ,)
      ],),

    ],);
  }


  //save and check key
  _incrementCounter(var id,{var keyUser}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if(id == 1){
      await prefs.setString('key', keyUser);
      print(keyUser);
    }else if(id == 5){
      setState(() {
        prefs.clear();
        key = null;
        userLogin.clear();
        massage = ' ';

      });
    }
    setState(() {
      key = prefs.getString('key') ?? null;
    });
  }

}
