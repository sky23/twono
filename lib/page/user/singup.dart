import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:twono/api/server.dart';
import 'package:twono/api/user.dart';
import 'package:twono/widget/header.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'HomeUser.dart';

//Home page for chose login or singUp
class SingUp extends StatefulWidget {
  @override
  _SingUpStat createState() => _SingUpStat();
}

class _SingUpStat extends State<SingUp> {
  var userLogin = new List<User>();
  var password;
  var massage = ' ';
  var user;
  var key;

  TextEditingController emailController = new TextEditingController();
  TextEditingController userController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _incrementCounter(2);
  }

  @override
  Widget build(BuildContext context) {


    print(userLogin.length);

    if(userLogin.length != 0 ?userLogin.first.status == 'Active' : false ){

      key==null ? _incrementCounter(1,keyUser: userLogin.first.key ) : null;

      setState(() {
        massage = ' $key  با موفقیت وارد شدید' ;

      });
    }else if (userLogin.length != 0 ? userLogin.first.status == 'fail' : false ){
      massage ='شکست در ورود به حساب کاربری';
    }

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(children: <Widget>[
          //start Column
          //Start Stack
          Stack(children: <Widget>[
            header(context,title: 'ایجاد حساب کاربری جدید',height: 150.0),


          ],),//end stack

          
          
          key==null ? fieldForSingUp(context) : Navigator.pop(context),

          //end Column
        ],),
      ),
    );
  }


  //send data user to server for check login **
  _singUp(var password , var user,var email ,var name) {

    Api.sendDataForSingUp(password,user,email,name).then((responseTwo) {
      if (this.mounted) {
        setState(() {
          Iterable listed = json.decode(responseTwo.body);
          userLogin = listed.map((model) => User.fromJson(model)).toList();
        });
      }
    });

  }

  // form for singUp (password and user)
  Widget fieldForSingUp(context){


    return  Column(children: <Widget>[
      Column(children: <Widget>[

        //user
        Padding(padding: EdgeInsets.only(top: 60.0),
          child: SizedBox(height: 60.0,width: 350.0,
            child: new TextField(
              controller: userController,
              decoration: const InputDecoration(
                  labelText: 'نام کاربری',
                  icon: const Padding(
                      padding: const EdgeInsets.only(
                          top: 15.0),
                      child: const Icon(Icons.account_box)
                  )
              ),

            ),
          ),
        ),


        //email
        Padding(padding: EdgeInsets.only(top: 5.0),
          child: SizedBox(height: 60.0,width: 350.0,
            child: new TextField(
              keyboardType: TextInputType.emailAddress,
              controller: emailController,
              decoration: const InputDecoration(
                  labelText: 'ایمیل',
                  icon: const Padding(
                      padding: const EdgeInsets.only(
                          top: 15.0),
                      child: const Icon(Icons.email)
                  )
              ),
              onChanged: (String valName ){
                setState(() {

                });
              },
            ),
          ),
        ),

        //password
        Padding(padding: EdgeInsets.only(top: 5.0),
          child: SizedBox(height: 60.0,width: 350.0,
            child: new TextField(
              controller: passwordController,
              decoration: const InputDecoration(
                  labelText: 'رمزعبور',
                  icon: const Padding(
                      padding: const EdgeInsets.only(
                          top: 15.0),
                      child: const Icon(Icons.lock)
                  )
              ),
              obscureText: true,
            ),
          ),
        ),



        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[

            CupertinoButton(
              borderRadius: BorderRadius.all(Radius.circular(28.0)),
              onPressed: (){


                if(validateEmail(emailController.text) != null)
                  setState(()=> massage = validateEmail(emailController.text));

                else if(validateUser(userController.text) != null)
                  setState(()=> massage = validateUser(userController.text));

                else if(validatePassword(passwordController.text) != null)
                  setState(()=> massage = validatePassword(passwordController.text));

                else{
                  setState(()=> massage = " ");
                _singUp(passwordController.text, userController.text , emailController.text , userController.text);

                }

              },
              child: new Text(' ثبت نام ',style: TextStyle(fontFamily: 'iran',fontSize: 14.0,color: Colors.blueAccent)),
            ),

          ],),


        Padding(
          padding: EdgeInsets.only(top: 10.0),
          child:new Text('$massage' , style: TextStyle(fontFamily: 'iran',color: Colors.redAccent),)
          ,)
      ],),

    ],);
  }



  //save and check key
  _incrementCounter(var id,{var keyUser}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if(id == 1){
      await prefs.setString('key', keyUser);
      print(keyUser);
    }else if(id == 5){
      setState(() {
        prefs.clear();
        key = null;
        userLogin.clear();
        massage = ' ';

      });
    }
    setState(() {
      key = prefs.getString('key') ?? null;
    });
  }


//************************ checked values *************************//
  String validateEmail(String value) {
    String pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      return "لطفا ایمیل خود را وارد کنید";

    } else if(!regExp.hasMatch(value)){

      return "ایمیل خود را صحیح وارد کنید";

    }else {
      return null;
    }
  }

  String validateUser(String value) {

    if (value.length == 0) {
      return "نام کاربری خود را وارد کنید";
    }else if(value.length <= 4 ){
      return " نام کاربری خود را بصورت صحیح وارد کنید";
    }else {
      return null;
    }
  }

  String validatePassword(String value) {

    if (value.length == 0) {
      return "رمز خود را وارد کنید";
    }else if(value.length <= 6 ){
      return " رمز شما کوتاه است";
    }else {
      return null;
    }
  }

}
