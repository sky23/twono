import 'dart:async';
import 'dart:io';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:twono/sql/ClientModel.dart';
import 'package:path_provider/path_provider.dart';

class DBProvider {
  DBProvider._();

  static final DBProvider db = DBProvider._();

  Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "notData2N.db");
    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
          await db.execute("CREATE TABLE note ("
              "id INTEGER PRIMARY KEY,"
              "title TEXT,"
              "content TEXT,"
              "color TEXT,"
              "target TEXT,"
              "date TEXT,"
              "special TEXT,"
              "keyPost TEXT,"
              "dateDelete TEXT"
              ")");
        });
  }

  newClient(Client newClient) async {
    final db = await database;
    //get the biggest id in the table
    var table = await db.rawQuery("SELECT MAX(id)+1 as id FROM note");
    int id = table.first["id"];
    //insert to the table using the new id
    var raw = await db.rawInsert(
        "INSERT Into note (id,title,content,color,special,target,date,keyPost,dateDelete)"
            " VALUES (?,?,?,?,?,?,?,?,?)",
        [id , newClient.title, newClient.content, newClient.color,newClient.special,newClient.target,newClient.date,newClient.keyPost,null]);
    return raw;
  }



  updateClient(Client newClient) async {
    final db = await database;
    var res = await db.update("note", newClient.toMap(),
        where: "id = ?", whereArgs: [newClient.id]);
    return res;
  }

  updateKeyPost(Client newClient) async {
    final db = await database;
    var x =  await db.rawUpdate(
        'UPDATE note SET keyPost = ${newClient.keyPost} WHERE id = ${newClient.id}'
    );

    return x;
  }


  getClient(int id) async {
    final db = await database;
    var res = await db.query("note", where: "id = ?", whereArgs: [id]);
    return res.isNotEmpty ? Client.fromMap(res.first) : null;
  }


  Future<List<Client>> getDataByWeekDay(int idWeek) async {
    final db = await database;
    var res = await db.query("note", where: "date = ? ORDER BY id DESC", whereArgs: [idWeek]);
    List<Client> list1 =
    res.isNotEmpty ? res.map((c) => Client.fromMap(c)).toList() : [];
    return list1;
  }

  Future<List<Client>> getByWhere(var where, var id ) async {
    final db = await database;
    var res = await db.query("note", where: "$where = ? ORDER BY id DESC", whereArgs: [id]);
    List<Client> list1 =
    res.isNotEmpty ? res.map((c) => Client.fromMap(c)).toList() : [];
    return list1;
  }

  Future<List<Client>> getSearch(String word ) async {
    final db = await database;

    var res = await db.query("note",where: "title LIKE '%$word%' OR content LIKE '%$word%' ");

    List<Client> list = res.isNotEmpty ? res.map((c) => Client.fromMap(c)).toList() : [];
    return list;
  }

  Future<List<Client>> getAllClients() async {
    final db = await database;
    var res = await db.query("note WHERE dateDelete IS NULL ORDER BY id DESC");
    List<Client> list = res.isNotEmpty ? res.map((c) => Client.fromMap(c)).toList() : [];

    return list;
  }

  Future<List<Client>> getNotKeyPosts() async {
    final db = await database;
    var res = await db.query("note",where: "keyPost LIKE 'null'");
    List<Client> list =
    res.isNotEmpty ? res.map((c) => Client.fromMap(c)).toList() : [];
    return list;
  }


  Future<List<Client>> getPostDelete() async {
    final db = await database;
    var res = await db.query("note",where: "dateDelete IS NOT NULL");
    List<Client> list =
    res.isNotEmpty ? res.map((c) => Client.fromMap(c)).toList() : [];
    return list;
  }


  deleteClient({int id,var keyPosts,var date, var net}) async {
    final db = await database;
    var x;

    if(keyPosts == 'null') {
      x = db.rawDelete("Delete from note WHERE id = $id");
    }else if(net == false ){
       x = await db.rawUpdate("UPDATE note SET dateDelete = '$date' WHERE id = $id");
    }else{
      x = db.rawDelete("Delete from note WHERE id = $id");
    }

    return x;

  }

  deleteClientAllInServer(var keyPost) async {
    final db = await database;

    return db.rawDelete("Delete from note WHERE keyPost = '$keyPost'");




  }



  deleteAll() async {
    final db = await database;
    db.rawDelete("Delete * from note");
  }
}