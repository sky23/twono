import 'dart:convert';

Client clientFromJson(String str) {
  final jsonData = json.decode(str);
  return Client.fromMap(jsonData);
}

String clientToJson(Client data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class Client {
  int id;
  String title;
  String content;
  String color;
  String special;
  String target;
  String date;
  String keyUser;
  String keyPost;
  String dateDelete;

  Client({
    this.id,
    this.title,
    this.content,
    this.color,
    this.special,
    this.target,
    this.date,
    this.keyUser,
    this.keyPost,
    this.dateDelete
  });

  factory Client.fromMap(Map<String, dynamic> json) => new Client(
    id:      json["id"],
    title:   json["title"],
    content: json["content"],
    color:   json["color"],
    special: json["special"],
    target:  json["target"],
    date:    json["date"],
    keyUser: json["keyuser"],
    keyPost: json["keyPost"],
    dateDelete: json["dateDelete"],

  );


  Map<String, dynamic> toMap() => {
    "id":      id,
    "title":   title,
    "content": content,
    "color":   color,
    "special": special,
    "target":  target,
    "date":    date,
    "keyPost" : keyPost
  };
}
