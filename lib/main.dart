import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'HomePage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '2No',
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      debugShowCheckedModeBanner: false,
      supportedLocales: [
        const Locale('fa',)
      ],
      locale: Locale('fa'),
      theme: ThemeData(
        fontFamily: 'irany',
        textTheme: TextTheme(display1: TextStyle(fontSize: 10.0,fontFamily: 'iran')),
        primarySwatch: Colors.blue,
      ),
      home: Material(child: HomePage(),),
    );
  }
}

