import 'package:flutter/material.dart';

/* Button For Drawer and ProductsList
*  Color Icons : (blue) Color.fromARGB(255, 1, 226, 252)
*  Color Text and Divider :  (blue) Color.fromARGB((80 and 255 ), 1, 179, 249)
* */
// ignore: must_be_immutable
class Btn extends StatelessWidget {
  var title;
  var iconsCustom;
  var color;
  VoidCallback onPressed;

  Btn({this.onPressed, this.title, this.iconsCustom, this.color});

  Widget build(BuildContext context) {
    // TODO: implement build
    return Directionality(
        textDirection: TextDirection.rtl,
        child: Column(
          children: <Widget>[
            MaterialButton(
                onPressed: onPressed,
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 10.0),
                      child: Icon(
                        iconsCustom,
                        color: Color.fromARGB(255, 1, 226, 252),
                      ),
                    ),
                    new Text(
                      title,
                      textAlign: TextAlign.right,
                      style: TextStyle(
                          fontFamily: 'iranm',
                          fontSize: 15.0,
                          color: Color.fromARGB(255, 1, 179, 249)),
                    ),

                  ],
                )),
            Divider(
              height: 0.001,
              color: Color.fromARGB(80, 1, 179, 249),
            ),
          ],
        ));
  }
}