import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:twono/api/server.dart';
import 'package:twono/page/EditPost.dart';
import 'package:twono/sql/Database.dart';
import 'package:twono/sql/ClientModel.dart';



class ListContent extends StatefulWidget {
  final special;
  final int idContext;
  final word;

  ListContent({this.special,this.idContext,this.word});
  @override
  _ListContentState createState() => _ListContentState(idContext: idContext,special: special,word: word);
}


class _ListContentState extends State<ListContent> {
  int idContext;
  var special;
  var word;
  var net = false;

  _ListContentState({this.idContext,this.special,this.word});


  //check net
  checkNet() async{
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        if (this.mounted)
          setState(() =>net= true);

      }
    } on SocketException catch (_) {
      if (this.mounted)
        setState(() => net = false);

    }
  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    checkNet();
  }

  var db;
  @override
  Widget build(BuildContext context) {

    if(idContext  == 0)
      db = DBProvider.db.getAllClients();
    else if (idContext == 9)
      db = DBProvider.db.getSearch(word);


    else
      db = DBProvider.db.getDataByWeekDay(idContext);


    return Padding(
      padding: EdgeInsets.only(top: 210.0),
      child: FutureBuilder<List<Client>>(
        future:db,
        builder: (BuildContext context, AsyncSnapshot<List<Client>> snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
              itemCount: snapshot.data.length ,
              itemBuilder: (BuildContext context, int index) {
                Client item = snapshot.data[index];
                return Dismissible(
                    key: UniqueKey(),
                    background: Container(color: Colors.red,
                        child: Padding(
                            padding: EdgeInsets.only(top: 10.0 , left: 20.0,right: 20.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Row(children: <Widget>[
                                  new Icon(Icons.chevron_right,color: Colors.white,),
                                  new Text('حذف',style: TextStyle(color: Colors.white),),
                                ],),
                                Row(children: <Widget>[
                                  new Icon(Icons.chevron_left,color: Colors.white,),
                                  new Text('حذف',style: TextStyle(color: Colors.white),),
                                ],),
                              ],)
                        )
                    ),
                    onDismissed: (direction) {

                      var dateDelete = new DateTime.now();

                      if(net && key != null){
                        print('net ok');
                        DBProvider.db.deleteClient(id: item.id,keyPosts: item.keyPost);

                        Client deleteRnx = Client(
                            id: item.id,
                            dateDelete: dateDelete.toString(),
                            keyPost: item.keyPost
                        );
                        Api.sendDateForDelete(deleteRnx);

                      }else{

                        if(item.keyPost =="null"){

                          DBProvider.db.deleteClient(id: item.id,keyPosts: item.keyPost);

                        }else{
                          DBProvider.db.deleteClient(id: item.id,keyPosts: item.keyPost, date: dateDelete.toString(),net: false);
                        }


                      }

                    },
                    child: special == null ?
                    listNote(
                        context, id: item.id, title: item.title,
                        contentShowInCard: item.content.length>35 ? '${item.content.substring(0,35)} ...' : item.content ,
                        content: item.content, color: item.color, date: item.date,
                        group: item.target, special: item.special, keyPost: item.keyPost
                    ):
                    item.special == special ? listNote(
                        context, id: item.id, title: item.title,
                        contentShowInCard: item.content.length>35 ? '${item.content.substring(0,35)} ...' : item.content ,
                        content: item.content, color: item.color, date: item.date, group: item.target,
                        special: item.special
                    ): null
                );
              },
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }
}



//search , color
class ListNote extends StatefulWidget {
  final color ;

  ListNote({this.color});

  @override
  _ListNoteState createState() => _ListNoteState(color: color);
}

class _ListNoteState extends State<ListNote> {
  var net = false;
  var color;
  var key;
  _ListNoteState({this.color});


  _incrementCounter() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      key = prefs.getString('key') ?? null;
    });
  }


  checkNet() async{
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        if (this.mounted)
          setState(() =>net= true);
      }
    } on SocketException catch (_) {
      if (this.mounted)
        setState(() =>net= false);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    checkNet();
    _incrementCounter();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: new Text('لیست نوشته ها', style: TextStyle(fontFamily: 'iran'),),
      ),
      body: Padding(
        padding: EdgeInsets.only(top: 10.0),
        child: FutureBuilder<List<Client>>(
          future: DBProvider.db.getByWhere('color', color),
          builder: (BuildContext context, AsyncSnapshot<List<Client>> snapshot) {


            if (snapshot.hasData) {
              return ListView.builder(
                itemCount: snapshot.data.length ,
                itemBuilder: (BuildContext context, int index) {
                  Client item = snapshot.data[index];
                  return Dismissible(
                      key: UniqueKey(),
                      background: Container(color: Colors.red,
                          child: Padding(
                              padding: EdgeInsets.only(top: 10.0 , left: 20.0,right: 20.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Row(children: <Widget>[
                                    new Icon(Icons.chevron_right,color: Colors.white,),
                                    new Text('حذف',style: TextStyle(color: Colors.white),),
                                  ],),
                                  Row(children: <Widget>[
                                    new Icon(Icons.chevron_left,color: Colors.white,),
                                    new Text('حذف',style: TextStyle(color: Colors.white),),
                                  ],),
                                ],)
                          )
                      ),
                      onDismissed: (direction) {

                        var dateDelete = new DateTime.now();

                        if(net){
                          print('net ok');
                          DBProvider.db.deleteClient(id:item.id,keyPosts: item.keyPost);

                          Client deleteRnx = Client(
                              id: item.id,
                              dateDelete: dateDelete.toString(),
                              keyPost: item.keyPost
                          );
                          Api.sendDateForDelete(deleteRnx);

                        }else{

                          if(item.keyPost =="null"){

                            DBProvider.db.deleteClient(id:item.id,keyPosts: item.keyPost);

                          }else{
                            DBProvider.db.deleteClient(id:item.id,keyPosts: item.keyPost, date: dateDelete.toString(),net: false);
                          }


                        }

                      },
                      child: color ==null ?
                      listNote(
                          context, id: item.id, title: item.title,
                          contentShowInCard: item.content.length>35 ? '${item.content.substring(0,35)} ...' : item.content ,
                          content: item.content, color: item.color, date: item.date,
                          group: item.target, special: item.special, keyPost: item.keyPost
                      ):
                      color == item.color ?
                      listNote(
                          context, id: item.id, title: item.title,
                          contentShowInCard: item.content.length>35 ? '${item.content.substring(0,35)} ...' : item.content ,
                          content: item.content, color: item.color, date: item.date,
                          group: item.target, special: item.special, keyPost: item.keyPost
                      ): null
                  );
                },
              );
            } else {
              return Center(child: CircularProgressIndicator());
            }



          },
        ),
      ),
    );
  }

}



Widget listNote(context, {var id,var title, var contentShowInCard,var content , var date, var color,var group ,var special,var keyPost}) {

  var colorSet ;
  var colorText;
  var weekDay;

  switch(color){
    case '1':colorSet = Colors.redAccent;colorText = Colors.white ;
    break;
    case '2':colorSet = Colors.green;colorText = Colors.white ;
    break;
    case '3':colorSet = Colors.black12;colorText = Colors.white ;
    break ;
    case '4' :colorSet = Colors.amber;colorText = Colors.white ;
    break;
    case '0':colorSet = Colors.white;colorText = Colors.redAccent;
    break;
  }

  switch(date){
    case '1':weekDay = 'دوشنبه';
    break;
    case '2':weekDay = 'سه شنبه';
    break;
    case '3':weekDay = 'چهار شنبه';
    break;
    case '4':weekDay = 'پنج شنبه';
    break;
    case '5':weekDay = 'جمعه';
    break;
    case '6':weekDay = 'شنبه';
    break;
    case '7':weekDay = 'یکشنبه';
    break;
  }



  return InkWell(
    onTap: (){

      Navigator.of(context).push(MaterialPageRoute(builder:(context)=>
          EditPost(idPost: id,title: title,content: content,color: color,group: group,special: special,data: date,)));

    },
    child: Card(
      elevation: 0.2,
      color: colorSet ,
      child: Column(
        children: <Widget>[
          Padding(
            padding:
            EdgeInsets.only(right: 15.0, top: 10.0, left: 15.0, bottom: 15.0),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    new Text(
                      '$title',
                      style: TextStyle(
                          color: colorText,
                          fontSize: 14.0,
                          fontFamily: 'iran'),
                    ),
                    //
                  ],
                ),
                Wrap(
                  children: <Widget>[
                    new Text(
                      contentShowInCard,
                      style: TextStyle(
                          color: Colors.black54,
                          fontFamily: 'iran',
                          fontSize: 10.0),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[

                    new Text(
                      '\t \t ${date==DateTime.now().weekday.toString() ? 'امروز': weekDay}',
                      style: TextStyle(
                          color: colorText , fontSize: 10.0, fontFamily: 'iran'),
                    ),

                    new Text(
                      '\t \t $group , ${keyPost=="null" ? 'افلاین' : 'انلاین'} ',
                      style: TextStyle(
                          color: colorText , fontSize: 10.0, fontFamily: 'iran'),
                    ),

                  ],
                )
              ],
            ),
          )
        ],
      ),
    ),
  );
}
