import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:twono/page/search/search.dart';
import 'package:twono/page/user/HomeUser.dart';
import 'ButtonCusttom.dart';
import 'List.dart';

//them for TextField
ThemeData appTheme =
ThemeData(primaryColor: Colors.blue, fontFamily: 'Montserrat');

//Shap Hearder
class CustomShapClipp extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final Path path = Path();
    path.lineTo(0.0, size.height - 45.0);

    var firstEndPoint = Offset(size.width * .5, size.height);
    var firstControlpoint = Offset(size.width * .20, size.height);
    path.quadraticBezierTo(firstControlpoint.dx, firstControlpoint.dy,
        firstEndPoint.dx, firstEndPoint.dy);

    var secondEndPoint = Offset(size.width, size.height - 45.0);
    var secondControlPoint = Offset(size.width * .80, size.height);
    path.quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy,
        secondEndPoint.dx, secondEndPoint.dy);

    path.lineTo(size.width, 0.0);

    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;
}



//Header Small for every page Except HomePage
Widget header(context,{height,title, bool back}) {
  back = back??false;

  return ClipPath(
    clipper: CustomShapClipp(),
    child: Container(
        height: height,
        decoration: BoxDecoration(
            gradient: LinearGradient(begin: Alignment.bottomLeft, colors: [
              Color.fromARGB(255, 1, 179, 249),
              Color.fromARGB(255, 1, 200, 252),
              Color.fromARGB(255, 1, 179, 249),
              Color.fromARGB(255, 1, 180, 249),
              Color.fromARGB(255, 1, 179, 249),
            ])),
        child: Padding(
          padding: EdgeInsets.only(top: 25.0, left: 10.0,right: 10.0),
          child: Column(
            children: <Widget>[
              Stack(children: <Widget>[


                back ? IconButton(icon: Icon(Icons.arrow_back,color: Colors.white,), onPressed: (){
                  Navigator.pop(context);
                }):SizedBox(width: 25.0,height: 25.0,),


                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[

                    Padding(
                      child: new Text(
                        title,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,
                            fontFamily: 'iran'),
                      ),
                      padding: EdgeInsets.only(top: 15.0, ),
                    ),


                  ],
                ),

              ],)

            ],
          ),
        )),
  );
}


//****************** Drawer Home page ********************

class AppDrawer extends StatefulWidget {
  @override
  _AppDrawerState createState() => new _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {

  var key;

  _incrementCounter() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      key = prefs.getString('key') ?? null;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _incrementCounter();
  }

  @override
  Widget build(BuildContext context) {
    var deviceSize = MediaQuery.of(context).size;

    //20
    var size20Height = deviceSize.height * 0.03125 ;

    print(size20Height*11);

    return Drawer(
      child: new ListView(
        children: <Widget>[

          Container(
              height: size20Height * 11,
              width: double.maxFinite,
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    colors: [
                      Color.fromARGB(255, 1, 226, 252),
                      Color.fromARGB(255, 1, 179, 249)
                    ],
                  )),
              child: Padding(
                padding: EdgeInsets.only(top: size20Height +10, right: size20Height + 5 ,left: 10.0),
                child: Column(
                  children: <Widget>[
                    Row(children: <Widget>[
                      ClipRRect(
                          borderRadius: BorderRadius.circular(100.0),
                          child:  Image.asset(
                            'assets/image/profile.jpg',
                            height: size20Height * 4 +10,
                            width: size20Height * 4 +10,
                          )
                      ),
                    ],),

                    Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Divider(height: size20Height ,color: Colors.white ,),
                        new Text(key == null ? 'شما وارد نشده اید' : 'شما وارد حساب کاربری خود شده اید',style: TextStyle(fontFamily: 'iranm',color: Colors.white,fontSize: 17.0 ),textAlign: TextAlign.right,),
                        Padding(padding: EdgeInsets.only(top: 5.0),
                          child: new Text(key == null ? 'کاربر مهمان' : 'کاربر فعال',style: TextStyle(fontFamily: 'iran',color: Colors.white),textAlign: TextAlign.right,)
                          ,),
                      ],)

                  ],
                ),
              )
          ),

          Column(children: <Widget>[
            Btn(title: 'حساب کاربری',onPressed: (){
              Navigator.of(context).push(
                  MaterialPageRoute(
                      builder: (context) => HomeUser()));
            },),
            Btn(title: 'جستو جو ',onPressed: (){
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => SearchPageList()));

            },),

            Row(children: <Widget>[
              Padding(padding: EdgeInsets.only(top: 15.0,right: 20.0),
                child:              new Text('نمایش بر اساس رنگ')
                ,)
            ],),
            Padding(
              padding: EdgeInsets.only(top: 15.0),
              child:Wrap(children: <Widget>[

                Padding(padding: EdgeInsets.only(right: 5.0,left: 5.0),
                  child: InkWell(child: new Container(height: 50.0,width: 50.0,color: Colors.redAccent,),
                    onTap: ()async{


                      NavigatorState navigator = Navigator.of(context);
                      Navigator.of(context).pop(); // Added

                      Route route = ModalRoute.of(context);
                      while (navigator.canPop()) navigator.removeRouteBelow(route);
                      await navigator.push(
                        MaterialPageRoute(
                          builder: (BuildContext context) => ListNote(color: '1',),
                        ),
                      );


                  //  Navigator.of(context).push(MaterialPageRoute(builder: (context)=>ListNote()));

                    },),
                ),


                Padding(padding: EdgeInsets.only(right: 5.0,left: 5.0),
                  child: InkWell(child: new Container(height: 50.0,width: 50.0,color: Colors.green,),
                    onTap: ()async{


                      NavigatorState navigator = Navigator.of(context);
                      Navigator.of(context).pop(); // Added

                      Route route = ModalRoute.of(context);
                      while (navigator.canPop()) navigator.removeRouteBelow(route);
                      await navigator.push(
                        MaterialPageRoute(
                          builder: (BuildContext context) => ListNote(color: '2',),
                        ),
                      );


                      //  Navigator.of(context).push(MaterialPageRoute(builder: (context)=>ListNote()));

                    },),
                ),


                Padding(padding: EdgeInsets.only(right: 5.0,left: 5.0),
                  child: InkWell(child: new Container(height: 50.0,width: 50.0,color: Colors.amber,),
                    onTap: ()async{


                      NavigatorState navigator = Navigator.of(context);
                      Navigator.of(context).pop(); // Added

                      Route route = ModalRoute.of(context);
                      while (navigator.canPop()) navigator.removeRouteBelow(route);
                      await navigator.push(
                        MaterialPageRoute(
                          builder: (BuildContext context) => ListNote(color: '4',),
                        ),
                      );


                      //  Navigator.of(context).push(MaterialPageRoute(builder: (context)=>ListNote()));

                    },),
                ),




              ],)
              ,)



          ],)

        ],
      ),
    );
  }
}