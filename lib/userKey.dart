import 'package:shared_preferences/shared_preferences.dart';

class keyCheker{
  var key;

  incrementCounter(var id,{var key}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if(id == 1){
      await prefs.setInt('key', key);
    }

    key = prefs.getString('key') ?? 'not';

    return key;
  }
}
